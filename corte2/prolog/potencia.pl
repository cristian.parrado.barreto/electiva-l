potencia(0,P,0):-P>0.
potencia(0,0,'Syntax error').

potencia(1,P,1).
potencia(X,0,1):-X>0.
potencia(X,P,R):-P1 is P-1, potencia(X,P1,P2), R is X*P2.
